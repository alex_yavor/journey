import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../../services';

@Component({
  selector: 'app-completed-journeys',
  templateUrl: './completed-journeys.component.html',
  styleUrls: ['./completed-journeys.component.css']
})
export class CompletedJourneysComponent implements OnInit {

  constructor(
    private profileService: ProfileService
  ) { }

  journeys;

  getJourneys(): void {
    this.profileService.completedJourneys()
      .subscribe(res => {
        this.journeys = Object.values(res.journeys);
        this.journeys[0].name = 'Как попутчик: ';
        this.journeys[1].name = 'Как водитель: ';
      })
  }

  ngOnInit() {
    this.getJourneys();
  }

}
