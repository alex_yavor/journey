--1.С


DROP FUNCTION findride(integer,text,text,timestamp without time zone,integer);
create or replace function findRide(user_id integer,departure_pnt text,arriavl_pnt text,departuretime timestamp,seats int default 1)
returns table(journeyId int,userId int,dr_name text,dr_surname text,age float,arrival_street text,departure_street text,arrival_point text,departure_point text,price_trip money,dep_time timestamp,curplaces smallint,places smallint) as $$
declare 
	q text:='
	select t.id,d.id,name,surname,extract (year from age(birthday)),arrivalstreet,departurestreet,arrivalpoint,departurepoint,price,departure,currseats,numofseats from userinfo d
	 left join auto a on a.id=d.auto
	join trip t on t.driver=d.id 
	where 	t.driver!= '||user_id||' and
		current_timestamp<=departure and
		currseats>='||seats;
 begin
	if (departuretime IS NOT NULL)then
		q:=q||	' and departure between ''' ||departuretime - interval '1 day' ||
		''' and '''||departuretime + interval '1 day'||'''';
	end if;
	if(departure_pnt !='') then
	 	q:=q||' and departurepoint = '''||departure_pnt||'''';
	 end if;
	 if(arriavl_pnt!='') then
		q:=q||' and arrivalpoint = '''||arriavl_pnt||'''';
	end if;
	
return query execute q;
end; 
 $$ LANGUAGE plpgsql;

DROP FUNCTION showuserinfo(integer);
create or replace function showuserinfo(userId integer) 
RETURNS TABLE(dName text,dSurname text,dAge float , markAuto text,modelAuto text,colorAuto text, trip bigint,Cgoodmark bigint,Cbadmarks bigint) as $$
declare 
	goodmarks bigint:=0 ;
	badmarks bigint:=0 ;
	trips bigint:= 0;
begin 
	goodmarks:=(select count(*)from feedbacktodriver  where mark>5 and driver=userId);
	goodmarks:=goodmarks+(select  count(*) from feedbacktocompanion  where mark>5 and companion=userId);
	badmarks:=(select count(*)from feedbacktodriver  where mark<=5 and driver=userId);
	badmarks:=badmarks+(select  count(*) from feedbacktocompanion  where mark<=5 and companion=userId);
	trips:=(select count(*)  from trip where driver=userId );
	trips:=trips+(select  count(*) from passangers  where companion=userId);
  	return query
  		
		select distinct name,surname,extract (year from age(birthday)),coalesce(a.mark, '') mark, coalesce(model, '') model, coalesce(color, '') color,coalesce(trips, 0)trips,
			goodmarks,badmarks
  			from userinfo d 
			left join auto a on(a.id=d.auto)
  			where userId=d.id;
end;
$$ LANGUAGE plpgsql;


create or replace function updateMycar(userid integer,mark text,model text,numberplate text,year date,typeauto text,color text)
returns void as 
$$
declare auto_id integer;
begin
	select auto into auto_id from userinfo where id=userid;
	if(auto_id notnull) then 
		update auto set mark=updateMycar.mark,model=updateMycar.model,numberplate=updateMycar.numberplate,year=updateMycar.year,typeauto=updateMycar.typeauto,color=updateMycar.color
		where id=auto_id;
	else 
		insert into auto (mark ,model ,numberplate ,year ,typeauto ,color ) 
			values(updateMycar.mark ,updateMycar.model ,updateMycar.numberplate ,updateMycar.year ,updateMycar.typeauto ,updateMycar.color)
				returning id into auto_id;
		update userinfo set auto=auto_id where id=userid;
	end if;
end; 
 $$ LANGUAGE plpgsql;


