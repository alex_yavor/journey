--авто
create table auto (
	id serial not null primary key,
    year date not null,
    mark text not null, 
    model text not null, 
    numberplate text not null unique, 
    typeauto text,
    color text not null
);
--пользователь
create table userInfo (
    id serial not null primary key,
    password text not null,
    name text not null,
    surname text not null,
    social text,
    phone text not null unique,
    email text not null unique,
    info text,
    birthday date not null,
    gender char(1) not null,
    auto integer unique references  auto
    on update cascade on delete cascade,
    is_admin boolean not null default false
);
--поездка
create table trip(
    id serial not null primary key,
	departure timestamp not null,
    arrival timestamp not null check(arrival>departure),
    arrivalstreet text not null,
    departurestreet text not null,
    numofseats smallint not null check(numofseats between 0 and 10),
    price money  not null,	
    departurepoint text not null,
    arrivalpoint text not null,
    driver  integer references userInfo
    on update cascade on delete cascade,
    currseats smallint not null check(numofseats between 0 and 10),
    iscompleted boolean default false,
    unique(driver,departure)
);

--пассажиры
create table passangers(
	trip integer not null references trip
    on update cascade on delete cascade,
    companion integer not null references userInfo
    on update cascade on delete cascade,
    primary key(trip,companion)
);
--отзыв о водителе
create table feedbacktodriver(
	id serial not null primary key,
    feedback text not null,
    mark  smallint not null check(mark between 1 and 10),
    driver integer not null references userInfo
    on update restrict on delete cascade,
    companion integer not null  check(driver<>companion) references userInfo
    on update restrict on delete cascade,
    unique(feedback,mark,driver,companion)
);
--отзыв о попутчике
create table feedbacktocompanion(
	id serial not null primary key,
    feedback text not null,
    mark  smallint not null check(mark between 1 and 10),
    driver integer not null references userInfo
    on update restrict on delete cascade,
    companion integer not null check(driver<>companion) references userInfo 
    on update restrict on delete cascade,
    unique(feedback,mark,driver,companion)
);

--пользователь чата
create table chat(
    member integer not null references userInfo
    on update restrict on delete cascade,
    id serial not null primary key
);
--chat message
create table messages(
    id serial not null primary key,
    chat  integer not null references chat
    on update restrict on delete cascade,
    content text not null,
    date timestamp not null,
    sender integer references userInfo
    on update restrict on delete cascade
);

create table message_status(
    message_id  integer not null references messages
    on update restrict on delete cascade,
    is_read boolean not null default false,
    user_id integer not null references userInfo
    on update restrict on delete cascade
)