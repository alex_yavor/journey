--drop view userJourneys;
create view userjourneys as 
select id,companion,departure,arrival,numofseats,driver,price,departurepoint,arrivalpoint,currseats,iscompleted 
        from passangers 
        join trip on trip=id ;
       

-- drop view journeyinfo;
create view journeyInfo as 
select d.id as userId ,t.id as journeyId,departure,arrival,numofseats,price,departurepoint,
    arrivalpoint,arrivalstreet,departurestreet,currseats,name,surname,extract (year from age(birthday)) as age ,phone,mark,model,numberplate,color,iscompleted 
    from trip t  join userinfo d on(driver=d.id) 
     left join auto a on d.auto=a.id ;

-- drop view journeyPassangers;
create view journeyPassangers as 
select id as userid,trip,name,surname,extract (year from age(birthday)) as age 
from passangers join userInfo on companion=id ;





--drop view showuserfeedback;
 create or replace view showuserfeedback as
select  companion as userid,driver as sender,feedback,mark,name,surname,extract (year from age(birthday)) from feedbacktocompanion fc join userinfo u on u.id=fc.driver 
union
select driver as userid,companion as sender, feedback,mark,name,surname,extract (year from age(birthday)) from feedbacktodriver fc join userinfo u on u.id=fc.companion 
;

--??????
--drop view populartrips;
create or replace view populartrips as
with cnt as(
select distinct departurepoint, arrivalpoint,count(*) over( partition by departurepoint, arrivalpoint) as cnts from trip 
)
,
max as (select distinct max(cnts) from cnt)

select  departurepoint, arrivalpoint from cnt where cnts=(select distinct max(cnts) from cnt) limit 5

	



