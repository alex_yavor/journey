create or replace function decreaseSeats() returns trigger as $$
begin	
	
	if exists(select currseats from trip t where t.id=NEW.trip and t.currseats::int=0) then
		RAISE EXCEPTION 'Все места заняты';	
	else 
	update trip t set currseats=currseats-1 where t.id=NEW.trip;
	end if;
	return new;
end;
$$ LANGUAGE plpgsql;

create  trigger decreaseSeats
before insert on passangers for each row
execute procedure decreaseSeats();


create or replace function checkTrip() returns trigger as $$
declare 
	d_trip integer;
 	c_trip integer;
begin
	select  t.id into d_trip from trip t  where t.driver=new.driver and  (new.departure between t.departure and t.arrival  or new.arrival between t.departure and t.arrival) and t.id<>new.id ;
	select trip into c_trip from passangers 
		join trip t on trip=id and (new.departure between t.departure and t.arrival  or new.arrival between t.departure and t.arrival) 
		where companion=new.driver;
	if (d_trip notnull) then
		raise exception 'Невозможно создать поездку, так как водитель будет находиться в поездке %',d_trip;
	elseif (c_trip notnull) then
		raise exception 'Невозможно создать поездку, так как водитель будет находиться в поездке %',c_trip;
	end if;
 return new;
 end;
 $$ LANGUAGE plpgsql;

create trigger checkDriverTrip
before insert or update on trip  for each row
execute procedure checkTrip();


create or replace function checkCompanionTrip() returns trigger as $$
declare 
	d_trip integer;
	c_trip integer;
begin
	select distinct t2.id into d_trip from trip t 
		join trip t2 on (t.departure between t2.departure and t2.arrival or t.arrival between t2.departure and t2.arrival) and t.id!=t2.id
		where t2.driver=new.companion and t.id=new.trip;
	select companion,t.departure,t.arrival,t2.departure,t2.arrival,t2.driver,trip into c_trip from passangers
		join trip t on t.id=trip  
		join trip t2 on  (t.departure between t2.departure and t2.arrival or t.arrival between t2.departure and t2.arrival) and t.driver=t2.driver and t.id!=t2.id
		where companion = new.companion;
			if(d_trip notnull) then
				raise exception '%',d_trip;
			elseif (c_trip notnull)  then
				raise exception '%',c_trip;
		end if;
	return new;
 end;
 $$ LANGUAGE plpgsql;

create trigger checkCompanionTrip
before insert or update on passangers  for each row
execute procedure checkCompanionTrip();


create or replace function increaseSeats() returns trigger as $$
begin	
	
	if exists(select currseats from trip t where t.id=old.trip and t.currseats=t.numofseats) then
		RAISE EXCEPTION 'Невозможно отменить бронь';	
	else 
	update trip t set currseats=currseats+1 where t.id=old.trip;
	end if;
	return old;
end;
$$ LANGUAGE plpgsql;

create  trigger increaseSeats
before delete on passangers for each row
execute procedure increaseSeats();


create or replace function checkDriver() returns trigger as $$
begin	
	if exists(select * from trip where driver=new.companion and id=new.trip) then
		RAISE EXCEPTION 'Водитель не может забронировать место у себя в поездке';	
	end if;
	return new;
end;
$$ LANGUAGE plpgsql;

create  trigger checkDriver
before insert or update on passangers for each row
execute procedure checkDriver();
