import { IRestData } from "../index.interface";
import { journey } from "./journey/journeys";
import { profile } from "./profile/profile";
import { otherRoutes } from "./other/other";

let paths: IRestData[] = [];
paths = paths.concat(journey, profile, otherRoutes);
export { paths };
