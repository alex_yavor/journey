import { IRestData } from "../../index.interface";
import {
    completedJourneys,
    upcomingJourneys,
    showMyProfile,
    updateMyProfile,
    myAuto,
    showUserInfo
} from './profile.controllers';
import { checkIfAuthenticated } from "../auth.functions";

export const profile: IRestData[] = [
    {
        description: `Получить свой профиль`,
        method: 'get',
        path: `profile`,
        controller: [checkIfAuthenticated, showMyProfile]
    },
    {
        description: `Просмотреть свои предстоящие поездки`,
        method: 'get',
        path: `profile/upcoming`,
        controller: [checkIfAuthenticated, upcomingJourneys]
    },
    {
        description: `Просмотреть свои совершенные поездки`,
        method: 'get',
        path: `profile/completed`,
        controller: [checkIfAuthenticated, completedJourneys]
    },
    {
        description: `Получить профиль по id`,
        method: 'get',
        path: `profile/:id`,
        controller: showUserInfo
    },
    {
        description: `Изменить свой профиль`,
        method: 'post',
        path: `profile`,
        controller: [checkIfAuthenticated, updateMyProfile]
    },
    {
        description: `Изменить данные (Создать) авто`,
        method: 'post',
        path: `profile/car`,
        controller: [checkIfAuthenticated, myAuto]
    },
]