import { getUserIdFromRequestHeaderAuth } from "../auth.functions";
import db from "../../../modules/database";
import { Request, Response, NextFunction } from 'express';

export let completedJourneys = async function (req: Request, res: Response, next: NextFunction) {

    try {
        console.log('comp')
        let userId = getUserIdFromRequestHeaderAuth(req.headers.authorization);
        //дата
        // let role = await db.query(`select role from userinfo where id=${userId}`);
        // role = role[0].role;

        let companionJourneys = await db.query(`select * from userjourneys where companion=${userId} and( iscompleted=true or arrival<current_date)`);
        let driverJourneys = await db.query(`select * from trip where driver=${userId} and (iscompleted=true or arrival<current_date)`);
        let journeys = { companionJourneys, driverJourneys }
        console.log({ journeys })
        res.send({ journeys });
    }
    catch (e) {
        res.status(500).send({ error: e });
        console.log(e);
    }
}

export let upcomingJourneys = async function (req, res, next) {
    try {
        console.log('up')
        console.log('hass',req.headers.authorization)
        let userId = getUserIdFromRequestHeaderAuth(req.headers.authorization);
        //дата
        console.log('hass2',userId)
        let companionJourneys = await db.query(`select * from userjourneys where companion=${userId} and iscompleted=false and arrival>=current_date`);
        let driverJourneys = await db.query(`select * from trip where driver=${userId}  and iscompleted=false and arrival>=current_date`);
        let journeys = { companionJourneys, driverJourneys }
        console.log({ journeys })
        res.send({ journeys });
    }
    catch (e) {
        console.trace(e)
        res.status(500).send({ error: e });
    }
}
export let showMyProfile = async function (req, res, next) {
    try {
        let userId = getUserIdFromRequestHeaderAuth(req.headers.authorization);
        let info = await db.query(`select auto,phone,name,surname,email,social,info,birthday,gender from userinfo where id=${userId}`);
        let auto = await db.query(`select year,mark,model,numberplate,typeauto,color from auto where id=${info[0].auto}`)
        delete info[0].auto;
        res.send({ info: info[0], auto: auto[0] })

    }
    catch (e) {
        console.log(e);
        res.status(500).send({ error: e });
    }
}

export let updateMyProfile = async function (req, res, next) {
    try {
        let userId = getUserIdFromRequestHeaderAuth(req.headers.authorization);
        let { gender, name, surname, email, phone, birthday, social, info } = req.body
        await db.query(`update userinfo set gender='${gender}',name='${name}',surname='${surname}',
        email='${email}',phone='${phone}',birthday='${new Date(birthday, 0, 1, 1).toLocaleDateString()}',
        social='${social}',info='${info}' where id=${userId}`)
        console.log(req.body)
        res.send({ message: 'Данные успешно изменены', success: true })

    }
    catch (e) {
        res.status(200).send({ message: 'Ошибка изменения данных' });
        console.log(e);
    }
}

export let myAuto = async function (req, res, next) {
    try {
        let userId = getUserIdFromRequestHeaderAuth(req.headers.authorization);
        let { mark, model, numberplate, year, typeauto, color } = req.body
        console.log('asdasd', [userId, mark, model, new Date(year, 0, 1, 1).toLocaleDateString(), typeauto, color])
        await db.func('updateMycar', [userId, mark, model,numberplate, new Date(year, 0, 1, 1).toLocaleDateString(), typeauto, color]);
        res.send({ message: 'Данные успешно изменены', success: true })
    }
    catch (e) {
        console.log(e);
        res.status(500).send({ error: e });
    }
}

export let showUserInfo = async function (req, res, next) {
    try {
        console.log('sss');
        //let userId = getUserIdFromRequestHeaderAuth(req.headers.authorization);
        let userId = req.params.id;
        let userIdFromReq = getUserIdFromRequestHeaderAuth(req.headers.authorization);
        if (userIdFromReq == userId) {
            res.send(null);

        }
        else {
            if (userId == 0)
                userId = userIdFromReq;
            // let role = await db.query(`select role from userinfo where id=${req.params.id}`);
            let table = 'showuserinfo';
            //if (role == 'companion')
            //      table = 'showcompanioninfo';
            let info = await db.func(table, userId);

            console.log('info', userId);
            let feedbacks = await db.query(`select * from showUserFeedback where userId=${userId}`);
            res.send({ info: info[0], feedbacks });
        }
    }
    catch (e) {
        console.log(e);
        res.status(500).send({ error: e });
    }
}