import * as expressJwt from 'express-jwt';
import * as fs from 'fs';
import * as jwt from 'jsonwebtoken';

const secret = 'thisismykey';
export let checkIfAuthenticated = expressJwt({
    secret: secret
});


export function getUserIdFromRequestHeaderAuth(auth) {
    if (!auth)
        return -1;
    auth = auth.slice(7)
    let decoded = jwt.decode(String(auth));
    if (!decoded)
        return -1;
    return +decoded['id']
}

export function createJWTToken(userId) {
    return jwt.sign({ id: `${userId}` }, secret, {
        expiresIn: 7200,
    })
}