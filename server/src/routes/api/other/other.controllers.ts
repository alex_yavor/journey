import db from "../../../modules/database";
import { createJWTToken, getUserIdFromRequestHeaderAuth } from "../auth.functions";
import * as bcrypt from 'bcrypt';

export let registerUser = function (req, res, next) {

    let salt = 10;
    console.log(req.body);
    bcrypt.hash(req.body.password, salt, async (err, hash) => {
        try {
            let userId = await db.one(`insert into userinfo(name ,surname  ,phone ,email ,
                birthday,gender ,password ) values('${req.body.name}' ,'${req.body.surname}' ,'${req.body.phone}'
                 ,'${req.body.email}' ,'${new Date(req.body.age, 0, 1, 1).toISOString()}','${req.body.gender}','${hash}') returning id`);
            console.log('userid', userId);
            if (userId.id) {
                const jwtBearerToken = createJWTToken(userId.id);
                res.status(200).json({
                    idToken: jwtBearerToken,
                    expiresIn: 120 * 60,
                    message: 'Профиль успешно создан',
                    success: true
                });
            }
        }
        catch (e) {
            console.log(e);
            res.send({ message: `Неверные данные` })
        }
    })


}

export const loginUser = async function (req, res, next) {
    try {
        const email = req.body.email;
        const password = req.body.password;
        const expiresIn = 7200;
        const info = (await db.query(`select * from userinfo where email='${email}'`))[0];
        bcrypt.compare(password, info.password, (err, samePasswords) => {
            if (samePasswords) {
                const jwtBearerToken = createJWTToken(info.id);
                res.send({
                    idToken: jwtBearerToken,
                    expiresIn,
                    success: true
                });
            }
            else {
                res.send({ message: 'Invalid login and/or password', success: false })
            }
        })
    }
    catch (e) {
        res.send({ message: e.message, success: false })
    }
}


export let leaveFeedback = async function (req, res, next) {
    try {
        let userId = getUserIdFromRequestHeaderAuth(req.headers.authorization);
        let feedback = req.body;
        let table = 'feedbacktodriver';
        if (feedback.role === 'companion')
            table = 'feedbacktocompanion'
        await db.query(`insert into ${table}(driver,companion,mark,feedback) 
            values(${userId},${feedback.userId},${feedback.mark},'${feedback.comment}')`)
        res.send({ message: 'Отзыв оставлен', success: true })
    }
    catch (e) {
        res.send({ message: 'Невозможно оставить отзыв', success: false })
    }
}


export let getPopularTrips = async function (req, res, next) {
    try {
        let trips = await db.query(`select * from populartrips`);
        console.log(trips);
        res.send(trips);

    }
    catch (e) {
        res.status(500).send({ error: e });
    }
}



