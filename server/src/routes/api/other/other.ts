import { IRestData } from "../../index.interface";
import { registerUser, loginUser, leaveFeedback, getPopularTrips } from "./other.controllers";
import { checkIfAuthenticated } from "../auth.functions";

export const otherRoutes: IRestData[] = [

    {
        description: `Регистрация`,
        method: 'post',
        path: `register`,
        controller: registerUser
    },
    {
        description: `Вход`,
        method: 'post',
        path: `login`,
        controller: loginUser
    },
    {
        description: `Оставить отзыв`,
        method: 'post',
        path: `leaveFeedback`,
        controller:  [checkIfAuthenticated,leaveFeedback]
    },
    {
        description: `Популярные маршруты`,
        method: 'get',
        path: `popular`,
        controller: getPopularTrips
    },
]