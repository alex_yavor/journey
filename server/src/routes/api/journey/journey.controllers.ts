import { getUserIdFromRequestHeaderAuth } from "../auth.functions";
import db from "../../../modules/database";
import { Request, Response, NextFunction } from 'express';

export let createNewJourney = async function (req, res, next) {
    try {
        let driver = getUserIdFromRequestHeaderAuth(req.headers.authorization);
        let { departure, arrival,
            numofseats, price,
            arrivalTime, departureTime,
            arrivalCity, departureCity,
            departureStreet, arrivalStreet
        } = req.body;
        arrivalTime = arrivalTime.split(':');
        departureTime = departureTime.split(':');
        arrival = new Date(arrival);
        departure = new Date(departure);
        arrival.setHours(arrivalTime[0])
        arrival.setMinutes(arrivalTime[1])
        departure.setHours(departureTime[0])
        departure.setMinutes(departureTime[1])

        await db.query(`insert into trip(arrival ,departure ,numofseats ,driver ,price ,arrivalpoint ,departurepoint,currseats,arrivalStreet,departureStreet)
         values('${new Date(arrival).toLocaleString()}', '${new Date(departure).toLocaleString()}', ${numofseats}, ${driver}, ${price},'${arrivalCity}', '${departureCity}', ${numofseats},'${arrivalStreet}','${departureStreet}')`);
        res.status(200).send({ message: 'Поездка успешно создана', success: true });
    }
    catch (e) {
        console.log(e);
        res.status(400).send({ message: e.message, success: false });
    }
}


export let completeJourney = async function (req, res, next) {
    try {
        let userId = getUserIdFromRequestHeaderAuth(req.headers.authorization);
        let journeyId = req.params.id;
        await db.query(`update trip set iscompleted=${true} where id=${journeyId} and driver=${userId}`);
        res.send({ message: 'Поездка завершена', success: true })

    }
    catch (e) {
        console.log(e);
        res.status(200).send({ message: 'Невозможно завершить поездку' });
    }
}


export let deleteJourney = async function (req, res, next) {
    try {
        let userId = getUserIdFromRequestHeaderAuth(req.headers.authorization);
        let journeyId = req.params.id;
        await db.query(`delete from trip where id=${journeyId} and driver=${userId}`);
        res.send({ message: 'Поездка удалена', success: true })
    }
    catch (e) {
        res.status(200).send({ message: 'Невозможно удалить поездку', success: true });
    }
}


export let getJourneys = async function (req: Request, res: Response, next: NextFunction) {
    console.log('qqq', req.query);
    let userId = getUserIdFromRequestHeaderAuth(req.headers.authorization);
    console.log('user', userId)
    let { arrivalPoint, departurePoint, departure, seats } = req.query;
    if (departure == 'null')
        departure = null;
    else departure = new Date(departure);
    if (departurePoint == 'null')
        departurePoint = '';
    if (arrivalPoint == 'null')
        arrivalPoint = '';

    let values = [userId, departurePoint, arrivalPoint, departure, +seats || 1]
    let journeys = await db.func(`findRide`, values);
    res.send(journeys);
}


export let getJourney = async function (req: Request, res: Response, next: NextFunction) {
    let userId = getUserIdFromRequestHeaderAuth(req.headers.authorization);
    console.log('req ', req.params.id)
    let role = null;
    let trip = await db.query(`select * from journeyInfo where journeyid=${req.params.id}`);
    if (trip[0].userid == userId)
        role = 'driver';
    let passangers = await db.query(`select * from journeyPassangers where trip=${req.params.id}`);
    console.log(passangers)
    if (passangers.length)
        passangers.forEach(passanger => {
            if (passanger.userid == userId)
                role = 'companion';
        });
    res.send({ trip: trip[0], passangers, role });
}


export let
    bookPlace = async function (req: Request, res: Response, next: NextFunction) {

        try {
            let userId = getUserIdFromRequestHeaderAuth(req.headers.authorization);

            console.log('booking', req.headers.authorization, req.params.id, userId)
            await db.query(`insert into passangers values(${req.params.id},${userId})`);
            let copmanionInfo = await db.query(`select name,surname,id as userid,extract (year from age(birthday)) as age from userinfo where id=${userId}`)
            res.status(200).send({ message: 'Бронь прошла успешно', success: true, userInfo: copmanionInfo[0] })
        }
        catch (e) {
            res.status(200).send({ message: e.message });
            console.log(e);
        }
    }


export let cancelJourney = async function (req, res, next) {
    try {
        let userId = getUserIdFromRequestHeaderAuth(req.headers.authorization);
        let journeyid = req.params.id;
        console.log('cancel j', userId, ' ', journeyid);

        await db.query(`delete from passangers where trip=${journeyid} and companion=${userId}`);
        res.send({ message: 'Ваша бронь успешно отменена', success: true, userId })

    }
    catch (e) {
        console.log(e)
        res.status(200).send({ message: e.message, success: false });
    }
}


export let cancelBooking = async function (req, res, next) {
    try {
        //let companionId=req.body
        let journeyId = req.params.journeyId;
        let companionId = req.params.id;
        await db.query(`delete from passangers where trip=${journeyId} and companion=${companionId}`);
        res.send({ message: 'Бронь успешно отменена', success: true })
        console.log('vololo', req.params);
    }
    catch (e) {
        console.log(e);
        res.send({ message: 'Невозможно отменить бронь', success: false })
    }
}