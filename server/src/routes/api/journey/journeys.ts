
import { IRestData } from "../../index.interface";
import {
    createNewJourney,
    completeJourney,
    deleteJourney,
    getJourneys,
    getJourney,
    bookPlace,
    cancelJourney,
    cancelBooking
} from './journey.controllers';
import { checkIfAuthenticated } from "../auth.functions";

export const journey: IRestData[] = [
    {
        description: `найти поездки по заданным критериям`,
        method: 'get',
        path: `journey`,
        controller: getJourneys
    },
    {
        description: `получить поездку по id`,
        method: 'get',
        path: `journey/:id`,
        controller: getJourney
    },
    {
        description: `Создать поездку`,
        method: 'put',
        path: `journey`,
        controller:  [checkIfAuthenticated,createNewJourney]
    },
    {
        description: `Удалить  поездку`,
        method: 'delete',
        path: `journey/:id`,
        controller: [checkIfAuthenticated,deleteJourney]
    },
    {
        description: `Завершить поездку`,
        method: 'patch',
        path: `journey/:id`,
        controller:  [checkIfAuthenticated,completeJourney]
    },
    {
        description: `Забронировать место в поездке`,
        method: 'post',
        path: `journey/:id`,
        controller: [checkIfAuthenticated, bookPlace]
    }, 
    {
        description: `отменить бронь попутчиком`,
        method: 'get',
        path: `journey/:id/cancel`,
        controller: [checkIfAuthenticated,cancelJourney]
    },
    {
        description: `отменить бронь водителем`,
        method: 'delete',
        path: `journey/:journeyId/:id`,
        controller:  [checkIfAuthenticated,cancelBooking]
    },
]