import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as path from 'path';
import { IRestData } from './../../routes/index.interface';

export default class Server {


    private app: express.Application;

    constructor(
        private port: number,
        private restData: IRestData[],
        private apiPath: string = 'api'
    ) {
        this.app = express()
        this.init()
    }


    private init() {
        this.app.set('port', this.port);
        this.initMiddleware();
        this.initRoutes();
    }


    public start() {
        this.app.listen(this.port, () => {
            console.info(`Server listening on port ${this.port}`);
        });
    }


    private initMiddleware(): void {
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use(express.static(path.join(__dirname, '../../public')));
        this.app.use(function (req, res, next) {

            // Website you wish to allow to connect
            res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');

            // Request methods you wish to allow
            res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

            // Request headers you wish to allow
            res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

            // Set to true if you need the website to include cookies in the requests sent
            // to the API (e.g. in case you use sessions)
            res.setHeader('Access-Control-Allow-Credentials', 'false');

            // Pass to next layer of middleware
            next();
        });
    }


    private initRoutes(): void {
        this.app.get('/', (req, res, next) => {
            res.sendFile(path.join(__dirname, '../../index.html'));
        });
        this.restData.forEach((p: IRestData) => {
            this.addRoutes(p.method, p.path, p.controller);
        })

    }

    private addRoutes(method: string = 'post', route: string, handler: Function): void {
        let endpointPath = `/${this.apiPath}/${route}`;
        this.app[method](`${endpointPath}`, handler);
    }
}