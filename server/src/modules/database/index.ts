import { IMain, IDatabase } from 'pg-promise';
import * as pgPromise from 'pg-promise';
import { dbOptions } from '../../settings';

const pgp: IMain = pgPromise();
const db: IDatabase<any> = pgp(dbOptions);

export default db;

