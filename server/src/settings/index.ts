export { dbOptions } from "./db.settings";

export const appSettings = {
    port: 3000,
    apiPath: 'api'
};

