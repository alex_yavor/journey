import Server from "./modules/server";
import { paths } from "./routes/api";
import { appSettings } from "./settings";



let server = new Server(appSettings.port, paths);
server.start();

